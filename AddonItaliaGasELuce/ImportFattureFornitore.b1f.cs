﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAPbouiCOM.Framework;
using System.IO;
using SAPbouiCOM;
using SAPbobsCOM;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Drawing;
using AddonItaliaGasELuce.ServiceLayer.SAPB1;
using System.Collections.ObjectModel;
using System.Data.Odbc;
using BusinessPatnerHelper;

namespace AddonItaliaGasELuce
{
    [FormAttribute("AddonItaliaGasELuce.ImportFattureFornitore", "ImportFattureFornitore.b1f")]
    class ImportFattureFornitore : UserFormBase
    {
        enum invoiceType
        {
            invoiceWithoutIVA,
            invoiceOverSoglia,
            InvoiceUnderSoglia
        }

        public ImportFattureFornitore()
        {
        }

        const double SOGLIA = 6410.26;
        const double SOGLIABOLLO = 77.47;
        private string NameFile = "";

        #region Element
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.Matrix Matrix0;
        private SAPbouiCOM.Button Button2;
        private EditText EditText0;
        private SAPbouiCOM.Button Button3;
        #endregion

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("Item_2").Specific));
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.Button2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button2_ClickBefore);
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.Button3 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.OnCustomInitialize();
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private void OnCustomInitialize()
        {

        }

        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            if (this.EditText0.Value == "") return;
            NameFile = this.EditText0.Value;
            if (!File.Exists(NameFile))
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("Il File non esiste!");
                return;
            }

            this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Clear();

            bool firstTime = true;
            try
            {
                StreamReader streamReader = new StreamReader(NameFile);
                while (!streamReader.EndOfStream)
                {
                    if (firstTime)
                    {
                        streamReader.ReadLine();
                        firstTime = false;
                        continue;
                    }
                    addRowMatrix(streamReader.ReadLine());
                }
            }
            catch (Exception e)
            {

                System.Windows.Forms.MessageBox.Show("Button0_ClickBefore: " + e.Message);
            }

            this.Matrix0.Columns.Item("Osserv").DataBind.Bind("DT_0", "Col_1");
            this.Matrix0.Columns.Item("Superam").DataBind.Bind("DT_0", "Col_2");
            this.Matrix0.Columns.Item("Payment_ID").DataBind.Bind("DT_0", "Col_3");
            this.Matrix0.Columns.Item("User_ID").DataBind.Bind("DT_0", "Col_4");
            this.Matrix0.Columns.Item("Name").DataBind.Bind("DT_0", "Col_5");
            this.Matrix0.Columns.Item("CF").DataBind.Bind("DT_0", "Col_6");
            this.Matrix0.Columns.Item("PIVA").DataBind.Bind("DT_0", "Col_7");
            this.Matrix0.Columns.Item("CODICESDI").DataBind.Bind("DT_0", "Col_8");
            this.Matrix0.Columns.Item("OR_PEC").DataBind.Bind("DT_0", "Col_9");
            this.Matrix0.Columns.Item("IBAN").DataBind.Bind("DT_0", "Col_10");
            this.Matrix0.Columns.Item("PaidAsTit").DataBind.Bind("DT_0", "Col_11");
            this.Matrix0.Columns.Item("AmWeek").DataBind.Bind("DT_0", "Col_12");
            this.Matrix0.Columns.Item("AmMouEar").DataBind.Bind("DT_0", "Col_13");
            this.Matrix0.Columns.Item("AmMoRes").DataBind.Bind("DT_0", "Col_14");
            this.Matrix0.Columns.Item("AmUnile").DataBind.Bind("DT_0", "Col_15");
            this.Matrix0.Columns.Item("AmGen").DataBind.Bind("DT_0", "Col_16");
            this.Matrix0.Columns.Item("OtherIn").DataBind.Bind("DT_0", "Col_17");
            this.Matrix0.Columns.Item("TotAmPaid").DataBind.Bind("DT_0", "Col_18");
            this.Matrix0.Columns.Item("DateGros").DataBind.Bind("DT_0", "Col_19");
            this.Matrix0.Columns.Item("6200noIVA").DataBind.Bind("DT_0", "Col_20");
            this.Matrix0.Columns.Item("PeriodRef").DataBind.Bind("DT_0", "Col_21");
            this.Matrix0.Columns.Item("ChargCard").DataBind.Bind("DT_0", "Col_22");
            this.Matrix0.Columns.Item("TecFeeSub").DataBind.Bind("DT_0", "Col_23");
            this.Matrix0.Columns.Item("RitIRPEF").DataBind.Bind("DT_0", "Col_24");
            this.Matrix0.Columns.Item("TaxVAT").DataBind.Bind("DT_0", "Col_25");
            this.Matrix0.Columns.Item("TaxINPS").DataBind.Bind("DT_0", "Col_26");
            this.Matrix0.Columns.Item("TotNetAm").DataBind.Bind("DT_0", "Col_27");
            this.Matrix0.Columns.Item("DateSent").DataBind.Bind("DT_0", "Col_28");
            this.Matrix0.Columns.Item("PlaToSDI").DataBind.Bind("DT_0", "Col_29");
            this.Matrix0.Columns.Item("BankTran").DataBind.Bind("DT_0", "Col_30");
            this.Matrix0.Columns.Item("Numero").DataBind.Bind("DT_0", "Col_31");
            this.Matrix0.Columns.Item("Bozza").DataBind.Bind("DT_0", "Col_32");
            this.Matrix0.LoadFromDataSource();

            this.Matrix0.Columns.Item("Osserv").BackColor = Color.Yellow.R | (Color.Yellow.G << 8) | (Color.Yellow.B << 16);
            this.Matrix0.AutoResizeColumns();
            checkData();
        }

        private void checkData()
        {
            try
            {
                //Dati anagrafici assenti / differenti???
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    string osservazioni = "";
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Name").Cells.Item(i).Specific)).Value.ToString() == "") osservazioni = osservazioni + " Nome Assente";
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Payment_ID").Cells.Item(i).Specific)).Value.ToString() == "") osservazioni = osservazioni + " PaymentID Assente";
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("User_ID").Cells.Item(i).Specific)).Value.ToString() == "") osservazioni = osservazioni + " UserID Assente";
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("IBAN").Cells.Item(i).Specific)).Value.ToString() == "") osservazioni = osservazioni + " IBAN Assente";
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(i).Specific)).Value.ToString() == "") osservazioni = osservazioni + " CF Assente";

                    ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value = (osservazioni.Length > 254 ? osservazioni.Substring(0, 254) : osservazioni);
                }

                //in osservazioni va: supera soglia / non la supera
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {

                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(i).Specific)).Value.ToString() != "")
                    {
                        OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT \"U_FO_SUPERAMENTO_SOGLIA\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"OCRD\" WHERE \"AddID\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(i).Specific)).Value.ToString().Trim() + "' ");
                        double soglia = 0;

                        if (!DbReader.HasRows || DbReader.IsDBNull(0) || DbReader["U_FO_SUPERAMENTO_SOGLIA"].ToString() == string.Empty) soglia = 0;
                        else soglia = Double.Parse(DbReader["U_FO_SUPERAMENTO_SOGLIA"].ToString());

                        if (DbReader.HasRows && soglia > SOGLIA)
                        {
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Superam").Cells.Item(i).Specific)).Value = "Si";
                        }
                        else
                        {
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Superam").Cells.Item(i).Specific)).Value = "No";
                        }

                    }

                }
            }
            catch (Exception e)
            {

                System.Windows.Forms.MessageBox.Show("checkData: " + e.Message);
            }
        }

        private void addRowMatrix(string row)
        {
            //System.Windows.Forms.MessageBox.Show("addRowMatrix: " + Program.oCompany.CompanyDB );
            try
            {
                OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'SEPARATORE_FILE' ");
                char[] s = { Convert.ToChar(DbReader["U_VALORE"].ToString()) };
                string[] columns = row.Split(s);
                try
                {
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Add();
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_3", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[0]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_4", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[1]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_5", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[2]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_6", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[3]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_7", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[4]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_8", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[5]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_9", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[6]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_10", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[7]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_11", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[8]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_12", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[9]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_13", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[10]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_14", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[11]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_15", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[12]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_16", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[13]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_17", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[14]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_18", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[15]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_19", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[16]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_20", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[17]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_21", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[18]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_22", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[19]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_23", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[20]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_24", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[21]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_25", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[22]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_26", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[23]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_27", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[24]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_28", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[25]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_29", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[26]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_30", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[27]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_31", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, columns[28]);
                    this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").SetValue("Col_32", this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count - 1, "");
                }
                catch (Exception e) { System.Windows.Forms.MessageBox.Show("addRowMatrix2: " + e.Message); }
            }
            catch (Exception ee)
            {

                System.Windows.Forms.MessageBox.Show("addRowMatrix1: " + ee.Message);
            }
        }

        private void Button1_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

                if (Matrix0.RowCount == 0) return;

                string resParam = checkParameters();
                if (resParam != "OK")
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(resParam);
                    return;
                }

                //Salvo se i documenti devono essere generati in bozza oppure no
                for (int i = 1; i <= this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count; i++)
                {
                    if (documentoBozza(i)) ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Bozza").Cells.Item(i).Specific)).Value = "SI";
                    else ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Bozza").Cells.Item(i).Specific)).Value = "NO";
                }

                // Creo/Aggiorno i BP
                OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT * FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@PARCONN\" ");
                string server = "";
                string dbuser = "";
                string dbpassword = "";
                string company = "";
                string user = "";
                string password = "";
                string dbtype = "";
                for (int k = 0; k < DbReader.RecordsAffected; k++)
                {
                    switch (DbReader.GetValue(DbReader.GetOrdinal("Code")).ToString()) //valore da nome colonna
                    {
                        case "SERVER":
                            server = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                        case "DBUSER":
                            dbuser = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                        case "DBPASSWORD":
                            dbpassword = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                        case "COMPANY":
                            company = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                        case "USER":
                            user = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                        case "PASSWORD":
                            password = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                        case "DBTYPE":
                            dbtype = DbReader.GetValue(DbReader.GetOrdinal("Name")).ToString(); //valore da nome colonna
                            DbReader.Read();
                            break;
                    }
                }
                Dictionary<int, string> resultsBP = new Dictionary<int, string>();
                resultsBP = BusinessPatnerHelper.BusinessPatnerHelper.createBPsFromFile(server, dbuser, dbpassword, company, user, password, dbtype, NameFile);
                foreach (var kvp in resultsBP)
                {
                    ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(kvp.Key).Specific)).Value += kvp.Value;
                }

                for (int i = 1; i <= this.UIAPIRawForm.DataSources.DataTables.Item("DT_0").Rows.Count; i++)
                {
                    bool bozza = true;
                    switch ((((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Bozza").Cells.Item(i).Specific)).Value.ToString()))
                    {
                        case "SI":
                            bozza = true;
                            break;
                        case "NO":
                            bozza = false;
                            break;
                    }

                    //agente senza partita Iva
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("PIVA").Cells.Item(i).Specific)).Value.ToString().Trim() == "")
                    {
                        string res = createInvoice(i, bozza, invoiceType.invoiceWithoutIVA);
                        if (res != "OK")
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value += " ERRORE " + res;
                        else
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value += " Inserimento doc OK";

                    }

                    //agente con partita Iva > 6400
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("PIVA").Cells.Item(i).Specific)).Value.ToString().Trim() != "" &&
                        ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Superam").Cells.Item(i).Specific)).Value.ToString() == "Si")
                    {
                        string res = createInvoice(i, bozza, invoiceType.invoiceOverSoglia);
                        if (res != "OK")
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value += res;
                        else
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value += " Inserimento doc OK";
                    }

                    //agente con partita Iva < 6400
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("PIVA").Cells.Item(i).Specific)).Value.ToString().Trim() != "" &&
                        ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Superam").Cells.Item(i).Specific)).Value.ToString() == "No")
                    {
                        string res = createInvoice(i, bozza, invoiceType.InvoiceUnderSoglia);
                        if (res != "OK")
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value += res;
                        else
                            ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(i).Specific)).Value += " Inserimento doc OK";
                    }
                }
            }
            catch (Exception e) { }

        }

        private string createInvoice(int indice, bool bozza, invoiceType type)
        {
            OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT COUNT(*) as COUNT FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"OCRD\"  WHERE \"AddID\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(indice).Specific)).Value.ToString().Trim() + "'");
            if (int.Parse(DbReader["COUNT"].ToString()) > 1) return " ERRORE: inserimento Doc. non riuscito ";

            ServiceLayer.SAPB1.Document oFatturaFornitore = new ServiceLayer.SAPB1.Document();
            ServiceLayer.SAPB1.BusinessPartner oBP = null;
            
            //Controllo che documento non esista già con stesso NumAtCard per stesso BP
            DbReader = ODBCUtility.doQuery("SELECT COUNT(*) as COUNT FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"OPCH\" WHERE \"CardCode\" = (SELECT \"CardCode\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".OCRD WHERE \"AddID\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(indice).Specific)).Value.ToString().Trim() + "') AND \"NumAtCard\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Numero").Cells.Item(indice).Specific)).Value.ToString() + "'");
            if (int.Parse(DbReader["COUNT"].ToString()) > 0) return "ERRORE: Esiste già un Documento con stesso BP e Numero Documento ";

            DbReader = ODBCUtility.doQuery("SELECT COUNT(*) as COUNT FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"ODRF\" WHERE \"CardCode\" = (SELECT \"CardCode\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".OCRD WHERE \"AddID\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(indice).Specific)).Value.ToString().Trim() + "') AND \"NumAtCard\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Numero").Cells.Item(indice).Specific)).Value.ToString() + "'");
            if (int.Parse(DbReader["COUNT"].ToString()) > 0) return "ERRORE: Esiste già un Documento in Bozza con stesso BP e Numero Documento ";

            int objectCode = 0;
            if (bozza)
            {
                objectCode = 112;
                oFatturaFornitore.DocObjectCode = "18"; // bozza
            }
            else
            {
                objectCode = 18;
            }

            try
            {
                DbReader = ODBCUtility.doQuery("SELECT \"CardCode\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".OCRD WHERE \"AddID\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(indice).Specific)).Value.ToString().Trim() + "' ");
                oBP = Program.currentOdataService.GetBP(DbReader["CardCode"].ToString());

                //CardCode
                oFatturaFornitore.CardCode = oBP.CardCode;

                //Tipo Servizi
                oFatturaFornitore.DocType = "S";

                //date
                oFatturaFornitore.DocDate = DateTime.Today;
                oFatturaFornitore.TaxDate = DateTime.Today;
                oFatturaFornitore.VatDate = DateTime.Today;

                //Serie
                string queryString = "";
                switch (type)
                {
                    case invoiceType.invoiceWithoutIVA:
                        queryString = "SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'SERIE_NO_IVA'  ";
                        break;
                    case invoiceType.InvoiceUnderSoglia:
                        queryString = "SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'SERIE_ELE_SOTTOSOGLIA'  ";
                        break;
                    case invoiceType.invoiceOverSoglia:
                        queryString = "SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'SERIE_ELE_SOPRASOGLIA'  ";
                        break;
                }
                DbReader = ODBCUtility.doQuery(queryString);
                oFatturaFornitore.Series = int.Parse(DbReader["U_VALORE"].ToString());

                //Numero della fattura nel NumAtCard
                oFatturaFornitore.NumAtCard = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Numero").Cells.Item(indice).Specific)).Value.ToString();

                //FLAG PER GENERAZIONE DOCUMENTI ELETTRONICI
                if (type == invoiceType.invoiceWithoutIVA) oFatturaFornitore.U_FLGXMLINV = "Y";

                //DocumentLines
                ServiceLayer.SAPB1.DocumentLine dl = new DocumentLine();

                //Coge
                switch (type)
                {
                    case invoiceType.invoiceWithoutIVA:
                        queryString = "SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'COGE_NO_IVA' ";
                        break;
                    case invoiceType.InvoiceUnderSoglia:
                        queryString = "SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'COGE_IVA_SOTTO_SOGLIA'";
                        break;
                    case invoiceType.invoiceOverSoglia:
                        queryString = "SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'COGE_IVA_SOPRA_SOGLIA'";
                        break;
                }
                DbReader = ODBCUtility.doQuery(queryString);
                dl.AccountCode = DbReader["U_VALORE"].ToString();

                //Periodo di riferimento
                dl.ItemDescription = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("PeriodRef").Cells.Item(indice).Specific)).Value.ToString();

                //Importo
                double importo = Double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("TotAmPaid").Cells.Item(indice).Specific)).Value.ToString());
                dl.LineTotal = importo;
                dl.VatGroup = oBP.VatGroup;

                //Eventuale Bollo
                if (type == invoiceType.invoiceWithoutIVA && importo > SOGLIABOLLO)
                {
                    ServiceLayer.SAPB1.DocumentAdditionalExpense addittionalExpensesLine = new DocumentAdditionalExpense();
                    addittionalExpensesLine.LineTotal = -2.0;
                    DbReader = ODBCUtility.doQuery("SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'CODICE_BOLLO'");
                    addittionalExpensesLine.ExpenseCode = int.Parse(DbReader["U_VALORE"].ToString());
                    oFatturaFornitore.DocumentAdditionalExpenses.Add(addittionalExpensesLine);
                    oFatturaFornitore.Comments = "Imposta di bollo assolta in modo virtuale con autorizzazione N.87430 del 20/12/2021 rilasciata dall' Agenzia delle Entrate U.T. Viareggio";

                }

                dl.WTLiable = "Y";
                ObservableCollection<ServiceLayer.SAPB1.WithholdingTaxData> lstWitholdingTax = new ObservableCollection<ServiceLayer.SAPB1.WithholdingTaxData>();

                //Ritenute IRPEF (comune a tutti i tipi)
                ServiceLayer.SAPB1.WithholdingTaxData wtl = new ServiceLayer.SAPB1.WithholdingTaxData();
                DbReader = ODBCUtility.doQuery("SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'CODE_RIT_IRPEF'");
                wtl.WTCode = DbReader["U_VALORE"].ToString();
                lstWitholdingTax.Add(wtl);

                //RitenutaINPS
                if (type == invoiceType.invoiceOverSoglia)
                {
                    ServiceLayer.SAPB1.WithholdingTaxData wtlINPS = new ServiceLayer.SAPB1.WithholdingTaxData();
                    DbReader = ODBCUtility.doQuery("SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'CODE_RIT_INPS'");
                    wtlINPS.WTCode = DbReader["U_VALORE"].ToString();
                    lstWitholdingTax.Add(wtlINPS);
                }

                //Aggiungo riga al Documento
                oFatturaFornitore.DocumentLines.Add(dl);
                oFatturaFornitore.U_COMMENTS = "Imported " + DateTime.Now.ToString() + " from file: " + NameFile + " row: " + indice;

                //Aggiunto ritenute al Documento
                oFatturaFornitore.WithholdingTaxDataCollection = lstWitholdingTax;

                //Aggiungo il Documento e aggiorno il BP
                //Program.currentOdataService.UpdateBP(oBP);
                Program.currentOdataService.AddNewDocument(oFatturaFornitore, objectCode);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return "OK";
        }

        private bool hasNecessaryInfo(int indice)
        {
            if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Osserv").Cells.Item(indice).Specific)).Value.ToString().Contains("CF")) return false;
            return true;
        }

        private string checkParameters()
        {

            OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" in ( 'CODE_RIT_INPS', 'CODE_RIT_IRPEF', 'CODICE_BOLLO', 'COGE_IVA_SOPRA_SOGLIA', 'COGE_IVA_SOTTO_SOGLIA', 'COGE_NO_IVA', 'SERIE_ELE_SOPRASOGLIA', 'SERIE_ELE_SOTTOSOGLIA', 'SERIE_NO_IVA','DOC_SEMPRE_BOZZA' ,'SERIE_BP', 'SEPARATORE_FILE' ) ");

            if (!DbReader.HasRows || DbReader.RecordsAffected != 12) return "Assenza di uno o più parametri: CODE_RIT_INPS, CODE_RIT_IRPEF, CODICE_BOLLO, COGE_IVA_SOPRA_SOGLIA, COGE_IVA_SOTTO_SOGLIA, COGE_NO_IVA , SERIE_ELE_SOPRASOGLIA, SERIE_ELE_SOTTOSOGLIA, SERIE_NO_IVA, DOC_SEMPRE_BOZZA , SERIE_BP, SEPARATORE_FILE";

            return "OK";
        }

        private void Button2_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            Thread t = new Thread(() =>
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                System.Windows.Forms.Form form = new System.Windows.Forms.Form();
                form.TopMost = true;
                form.TopLevel = true;
                form.Focus();
                DialogResult dr = openFileDialog.ShowDialog(form);

                if (dr == DialogResult.OK)
                {
                    string filePath = openFileDialog.FileName;
                    this.EditText0.Value = filePath;
                }
            });
            t.IsBackground = true;
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        private void Form_ResizeAfter(SBOItemEventArg pVal)
        {
            this.Button1.Item.Top = 15;
            this.Button1.Item.Left = 15;

            this.Button0.Item.Top = Button1.Item.Top;
            this.Button0.Item.Left = Button1.Item.Left + Button1.Item.Width + 10;

            this.Button2.Item.Top = Button0.Item.Top;
            this.Button2.Item.Left = Button0.Item.Left + Button0.Item.Width + 10;

            this.EditText0.Item.Top = Button2.Item.Top;
            this.EditText0.Item.Left = Button2.Item.Left + Button2.Item.Width + 10;

            this.Matrix0.Item.Top = Button1.Item.Top + Button1.Item.Height + 10;
            this.Matrix0.Item.Left = Button1.Item.Left;

            this.Button3.Item.Top = this.Matrix0.Item.Top + this.Matrix0.Item.Height;
            this.Button3.Item.Left = this.Matrix0.Item.Left + this.Matrix0.Item.Width - Button3.Item.Width - 20;
        }

        private bool documentoBozza(int i)
        {
            OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT \"U_VALORE\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"@ADDONPAR\" WHERE \"Code\" = 'DOC_SEMPRE_BOZZA'");

            if (DbReader.HasRows && Boolean.Parse(DbReader["U_VALORE"].ToString().ToLower())) return true;

            //BP ricercato per CF non esiste
            if (!existsBP_CF(i)) return true;

            //BP ha informazioni diverse (ABI / CAB) da quello da file importato
            DbReader = ODBCUtility.doQuery("SELECT \"dflIBAN\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"OCRD\" WHERE \"Name\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(i).Specific)).Value.ToString() + "' ");

            if (DbReader["dflIBAN"].ToString() != ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("IBAN").Cells.Item(i).Specific)).Value.ToString()) return true;

            return false;
        }

        private bool existsBP_CF(int i)
        {
            OdbcDataReader DbReader = ODBCUtility.doQuery("SELECT \"CardCode\" FROM " + " \"" + Program.oCompany.CompanyDB + "\" " + ".\"OCRD\" WHERE \"Name\" = '" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("CF").Cells.Item(i).Specific)).Value.ToString() + "' ");
            if (!DbReader.HasRows || DbReader["CardCode"].ToString() == "") return false;

            return true;
        }

    }
}

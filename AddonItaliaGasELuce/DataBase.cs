﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManager;

namespace AddonItaliaGasELuce
{
    class DataBase : Db
    {
        public DataBase()
        {
            Tables = new DbTable[] {
                new DbTable("@ADDONPAR", "Parametri AddOn", SAPbobsCOM.BoUTBTableType.bott_NoObject),
                new DbTable("@PARCONN", "Parametri AddOn", SAPbobsCOM.BoUTBTableType.bott_NoObject),
            };

            Columns = new DbColumn[] {
                
                #region FOADDONPAR Parametri addon
                new DbColumn("@ADDONPAR", "VALOREDEC", "Valore decimale",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10, new DbValidValue[0],-1,null),
                  new DbColumn("@ADDONPAR", "VALORE", "Valore",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, new DbValidValue[0],-1,null),
                #endregion

                #region FOADDONPAR Parametri addon
                  //si gestisce tutto con NOME e VALORE
                #endregion
                    
              #region OCRD Anagrafica BP
                new DbColumn("OCRD", "FO_SUPERAMENTO_SOGLIA", "Superamento Soglia",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10,new DbValidValue[0],-1,null),
                #endregion
 
                #region OCRD Anagrafica BP
                new DbColumn("OWHT", "FO_TIPORIT", "Tipo Ritenuta",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50,new DbValidValue[0],-1,null),
                #endregion

               new DbColumn("OPCH", "FLGXMLINV", "Generazione XML",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 0, new DbValidValue[]{ new DbValidValue("N","No"), new DbValidValue("Y","Si")}, 0,null),
               new DbColumn("OPCH", "COMMENTS", "Commenti Importazione",
                    SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10,new DbValidValue[0],-1,null),

            };

        }
    }
}

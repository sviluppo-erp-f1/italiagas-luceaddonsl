﻿using System;
using System.Data.Odbc;
using SAPbouiCOM.Framework;
using System.Xml;
using System.IO;

namespace AddonItaliaGasELuce
{

    class Program
    {
        public static SAPbobsCOM.Company oCompany;
        public static SAPbobsCOM.SBObob oSBObob;
        public static SAPbobsCOM.CompanyService oCompanyService;
        public static string serviceLayerAddress;
        public static string sConnectionContext;

        public static string strCurrentServiceURL = string.Empty;
        public static ServiceLayerService currentOdataService = ServiceLayerService.GetInstance();
        public static OdbcConnection ODBCconnection = null;
        //public static string companyDB = "TEST_MM";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    //If you want to use an add-on identifier for the development license, you can specify an add-on identifier string as the second parameter.
                    //oApp = new Application(args[0], "XXXXX");
                    oApp = new Application(args[0]);
                }
                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();

                oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
                oSBObob = (SAPbobsCOM.SBObob)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
                oCompanyService = oCompany.GetCompanyService();

                setServiceLayer();
                connessioneODBC();

                DataBase db = new DataBase();
                if (db.check(Program.oCompany))
                {
                    db.Add(oCompany);
                    db.valorizzaParametro(Program.oCompany);
                }

                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                oApp.Run();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        static void connessioneODBC()
        {
            XmlDocument docXml = new XmlDocument();
            docXml.Load(Directory.GetCurrentDirectory() + @"\IGL_parametriDB.xml");
            XmlNodeList elemList = docXml.GetElementsByTagName("ParametriConnessioneDB");
            string _strServerName = string.Empty;
            string _strLoginName = string.Empty;
            string _strPassword = string.Empty;
            string _strCompDB = string.Empty;
            string strConnectionString = string.Empty;

            foreach (XmlNode node in elemList)
            {
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    _strServerName = childNode.Attributes["Server"].Value;
                    _strLoginName = childNode.Attributes["DbUserName"].Value;
                    _strPassword = childNode.Attributes["DbPassword"].Value;
                    //_strCompDB = childNode.Attributes["CompanyDB"].Value;
                    _strCompDB = oCompany.CompanyDB;
                }
            }


            if (IntPtr.Size == 8)
            {
                strConnectionString = string.Concat(strConnectionString, "Driver={HDBODBC};");
            }
            else
            {
                strConnectionString = string.Concat(strConnectionString, "Driver={HDBODBC32};");
            }

            strConnectionString = string.Concat(strConnectionString, "SERVERNODE=", _strServerName, ";");
            strConnectionString = string.Concat(strConnectionString, "DATABASE=", _strCompDB, ";");
            strConnectionString = string.Concat(strConnectionString, "UID=", _strLoginName, ";");
            strConnectionString = string.Concat(strConnectionString, "PWD=", _strPassword, ";");

            ODBCconnection = new OdbcConnection(strConnectionString.ToString());
            ODBCconnection.Open();
            /*
            string queryString = "SELECT \"CardName\" FROM TEST_MM.OCRD WHERE \"CardType\"='S' ";
            OdbcCommand DbCommand = new OdbcCommand(queryString, ODBCconnection);
            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            
            while (DbReader.Read())
            {
                string s = DbReader["CardName"].ToString();

            }


            int fCount = DbReader.FieldCount;
            Console.Write(":");
            for (int i = 0; i < fCount; i++)
            {
                String fName = DbReader.GetName(i);
                Console.Write(fName + ":");
            }
            Console.WriteLine();

            while (DbReader.Read())
            {
                Console.Write(":");
                for (int i = 0; i < fCount; i++)
                {
                    String col = DbReader.GetString(i);

                    Console.Write(col + ":");
                }
                Console.WriteLine();
            }

    */
        }

        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    //Exit Add-On
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    break;
                default:
                    break;
            }
        }

        private static void setServiceLayer()
        {
            //
            //string server = Application.SBO_Application.Company.ServerName.Substring(0, Application.SBO_Application.Company.ServerName.IndexOf(":"));
            //strCurrentServiceURL = "https://" + server + ":50000/b1s/v1";

            string server = oCompany.SLDServer.Substring(0, oCompany.LicenseServer.IndexOf(":"));
            strCurrentServiceURL = "https://" + server + ":50000/b1s/v1";

            currentOdataService.InitServiceContainer(strCurrentServiceURL);
            sConnectionContext = Application.SBO_Application.Company.GetServiceLayerConnectionContext(strCurrentServiceURL);
            string[] cookieItems = sConnectionContext.Split(';');

            foreach (var cookieItem in cookieItems)
            {
                string[] parts = cookieItem.Split('=');
                if (parts.Length == 2)
                {
                    if (parts[0] == "B1SESSION")
                    {
                        currentOdataService.strCurrentSessionGUID = parts[1];
                    }
                }
            }
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddonItaliaGasELuce
{
    static class ODBCUtility
    {
        public static OdbcDataReader doQuery(string queryString)
        {
            OdbcCommand DbCommand = new OdbcCommand(queryString, Program.ODBCconnection);
            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            DbReader.Read();
            return DbReader;
        }

        public static int countRecords(OdbcDataReader DBDataReader)
        {
            if (!DBDataReader.HasRows) return 0;
            int n = 0;
            while (DBDataReader.Read())
            {
                n++;
            }
            return n;
        }
    }
}